
import numpy as np
dataset_filename = "affinity_dataset.txt"
X = np.loadtxt(dataset_filename)
n_samples, n_features = X.shape
print("数据集有{0}样本和{1}特征".format(n_samples, n_features))


# 特征名称
features = ["面包", "牛奶", "奶酪", "苹果", "香蕉"]


from collections import defaultdict
valid_rules = defaultdict(int)
invalid_rules = defaultdict(int)
num_occurences = defaultdict(int)

for sample in X:
    for premise in range(n_features):
        if sample[premise] == 0: continue
        # 记录条件产品
        num_occurences[premise] += 1
        for conclusion in range(n_features):
            if premise == conclusion:  #比如计算，同时买苹果和苹果，没意义.
                continue
            if sample[conclusion] == 1: #购买结论商品
                valid_rules[(premise, conclusion)] += 1
            else:                      #没有购买结论商品
                invalid_rules[(premise, conclusion)] += 1
support = valid_rules
confidence = defaultdict(float)
for premise, conclusion in valid_rules.keys():
    confidence[(premise, conclusion)] = valid_rules[(premise, conclusion)] / num_occurences[premise]
	

def print_rule(premise, conclusion, support, confidence, features):
    premise_name = features[premise]
    conclusion_name = features[conclusion]
    print("规则: 某人购买{0}，也会购买{1}".format(premise_name, conclusion_name))
    print("置信度:{0:.3f}".format(confidence[(premise, conclusion)]))
    print("支持度:{0}".format(support[(premise, conclusion)]))
    print("")

	
from operator import itemgetter
sorted_support = sorted(support.items(), key=itemgetter(1), reverse=True)

for index in range(5):
    print("Rule #{0}".format(index + 1))
    (premise, conclusion) = sorted_support[index][0]
    print_rule(premise, conclusion, support, confidence, features)
	
sorted_confidence = sorted(confidence.items(), key=itemgetter(1), reverse=True)	

for index in range(5):
    print("Rule #{0}".format(index + 1))
    (premise, conclusion) = sorted_confidence[index][0]
    print_rule(premise, conclusion, support, confidence, features)
