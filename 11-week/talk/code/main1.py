

import csv
import numpy as np
import os

#准备数组对象
#已知道数据大小
X = np.zeros((351, 34), dtype='float')
y = np.zeros((351,), dtype='bool')

#得到主目录
home_folder = os.path.expanduser("~")
#print(home_folder)


#数据文件路径
data_folder = os.path.join(home_folder, "data")
data_filename = os.path.join(data_folder, "ionosphere.data")
#print(data_filename)


with open(data_filename, 'r') as input_file:
    reader = csv.reader(input_file)
    for i, row in enumerate(reader):
        # Get the data, converting each item to a float
        data = [float(datum) for datum in row[:-1]]
        # Set the appropriate row in our dataset
        X[i] = data
        #print("++++>",data)
        # 1 if the class is 'g', 0 otherwise
        y[i] = row[-1] == 'g'


#把数据切为训练和测试
from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=14)


'''
print("There are {} samples in the training dataset".format(X_train.shape[0]))
print("There are {} samples in the testing dataset".format(X_test.shape[0]))
print("Each sample has {} features".format(X_train.shape[1]))
'''

#导入分类器
from sklearn.neighbors import KNeighborsClassifier

#创建估计器
estimator = KNeighborsClassifier()


''' 简单的分类
#训练估计器
estimator.fit(X_train, y_train)
#测试
y_predicted = estimator.predict(X_test)
accuracy = np.mean(y_test == y_predicted) * 100
print("The accuracy is {0:.1f}%".format(accuracy))
'''

'''交叉检验
from sklearn.model_selection import cross_val_score
# put paramter cv=5 to try 5 times
scores = cross_val_score(estimator, X, y, scoring='accuracy')
print(scores)
average_accuracy = np.mean(scores) * 100
print("The average accuracy is {0:.1f}%".format(average_accuracy))
'''

'''
from sklearn.model_selection import cross_val_score
avg_scores = []
all_scores = []
parameter_values = list(range(1, 21))  # Including 20
for n_neighbors in parameter_values:
    estimator = KNeighborsClassifier(n_neighbors=n_neighbors) #设置算法参数
    scores = cross_val_score(estimator, X, y, scoring='accuracy')
    all_scores.append(scores)          #交叉检验中的各个准确率
    avg_scores.append(np.mean(scores)) #平均准确率
    
print(avg_scores)
print(all_scores)
'''

''' 进行简单的预处理
from sklearn.model_selection import cross_val_score
X_broken = np.array(X)
X_broken[:,::2] /= 10
estimator = KNeighborsClassifier()
original_scores = cross_val_score(estimator, X, y,scoring='accuracy')
print("The original average accuracy for is {0:.1f}%".format(np.mean(original_scores) * 100))
broken_scores = cross_val_score(estimator, X_broken, y,scoring='accuracy')
print("The 'broken' average accuracy for is {0:.1f}%".format(np.mean(broken_scores) * 100))
'''

'''
#标准预处理
from sklearn.model_selection import cross_val_score
from sklearn.preprocessing import MinMaxScaler
X_broken = np.array(X)
X_transformed = MinMaxScaler().fit_transform(X)
X_transformed = MinMaxScaler().fit_transform(X_broken)
estimator = KNeighborsClassifier()
transformed_scores = cross_val_score(estimator, X_transformed, y, scoring='accuracy')
print("The average accuracy for is {0:.1f}%".format(np.mean(transformed_scores) * 100))
'''

'''
#使用流水线
from sklearn.model_selection import cross_val_score
from sklearn.preprocessing import MinMaxScaler
from sklearn.pipeline import Pipeline
X_broken = np.array(X)
scaling_pipeline = Pipeline([('scale', MinMaxScaler()),
                             ('predict', KNeighborsClassifier())])

scores = cross_val_score(scaling_pipeline, X_broken, y, scoring='accuracy')
print("The pipeline scored an average accuracy for is {0:.1f}%".format(np.mean(scores) * 100))
'''








