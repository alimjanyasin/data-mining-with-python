
import numpy as np


# let's load from a txt 
import numpy as np
data = np.loadtxt("iris.txt",delimiter=',')
X=np.array(data[:,[0,1,2,3]])
y= data[:,4]

n_samples, n_features = X.shape

print(data)
print(y)

''' # the code from the book
#load data
from sklearn.datasets import load_iris
dataset = load_iris()
X = dataset.data
y = dataset.target
n_samples, n_features = X.shape
'''


# make deature discreate
attribute_means = X.mean(axis=0)
assert attribute_means.shape == (n_features,)
X_d = np.array(X >= attribute_means, dtype='int')



# split data into training and test
# run the following first, replace model
# run "pip install sklearn" 
# "model_selection" replaces "cross_validation"

from sklearn.model_selection import train_test_split
random_state = 14

X_train, X_test, y_train, y_test = train_test_split(X_d, y, random_state=random_state)
#print("There are {0} training samples".format(y_train.shape))
#print("There are {0} testing samples".format(y_test.shape))



from collections import defaultdict
from operator import itemgetter


def train(X, y_true, feature):
    
    (n_samples, n_features) = X.shape
    
    assert 0 <= feature < n_features
    # get a coloum 
    values = set(X[:,feature])
    # Stores the predictors array that is returned
    predictors = dict()
    errors = []
    for current_value in values:
        #print(current_value)
        most_frequent_class, error = train_feature_value(X, y_true, feature, current_value)

        predictors[current_value] = most_frequent_class
        errors.append(error)
    #Compute the total error of using this feature to classify on
    total_error = sum(errors)
    print("++++++",predictors, total_error)
    return predictors, total_error
    

def train_feature_value(X, y_true, feature, value):
    print("--------------")
    print("(%d,%d)"%(feature, value))
    # Create a simple dictionary to count how frequency they give certain predictions
    class_counts = defaultdict(int)
    # Iterate through each sample and count the frequency of each class/value pair
    for sample, y in zip(X, y_true):
        if sample[feature] == value:
            class_counts[y] += 1
    
    # Now get the best one by sorting (highest first) and choosing the first item
    sorted_class_counts = sorted(class_counts.items(), key=itemgetter(1), reverse=True)
    print("sorted_class_counts",sorted_class_counts)
    most_frequent_class = sorted_class_counts[0][0]
    
    # The error is the number of samples that do not classify as the most frequent class
    # *and* have the feature value.
    
    #sum of errors
    error = 0
    for class_value, class_count in class_counts.items():
        if(class_value != most_frequent_class):
            error = error + class_count
    #print(error)
    
    
    error = sum([class_count for class_value, class_count in class_counts.items()
                 if class_value != most_frequent_class])
    
    print("(most_frequent_class,error):",most_frequent_class,",",error)
    return most_frequent_class, error


#print(train(X_train, y_train,0))


all_predictors= {}  
errors={}

for variable in range(X_train.shape[1]):
    #print("calling train function",variable)
    #print("*********************************")
    all_predictors[variable]=train(X_train, y_train, variable)

for variable, (mapping, error) in all_predictors.items():
    errors[variable]=error  


print("all_predictors---->",all_predictors)
print("errors---->",errors)



# Now choose the best and save that as "model"
# Sort by error
best_variable, best_error = sorted(errors.items(), key=itemgetter(1))[0]
print("The best model is based on variable {0} and has error {1:.2f}".format(best_variable, best_error))


# Choose the best model
model = {'variable': best_variable,
         'predictor': all_predictors[best_variable][0]}
print(model)    



def predict(X_test, model):
    variable = model['variable']
    predictor = model['predictor']      
    y_predicted = np.array([predictor[int(sample[variable])] for sample in X_test])
    return y_predicted
    

y_predicted = predict(X_test, model)


# Compute the accuracy by taking the mean of the amounts that y_predicted is equal to y_test
accuracy = np.mean(y_predicted == y_test) * 100
print("The test accuracy is {:.1f}%".format(accuracy))  


