

#用pandas加载数据集
import os
import numpy as np
import pandas as pd

data_filename = os.path.join("C:\python\data","matchings.csv")
#print(data_filename)


#pandas加载数据（不修改数据）
results = pd.read_csv(data_filename)
#print(results.loc[:10])


#pandas加载数据（修改数据）
# Don't read the first row, as it is blank, and parse the date column as a date
results = pd.read_csv(data_filename, parse_dates={'Date':[0, 1]}, skiprows=[0,])
# Fix the name of the columns
results.columns = ["Date", "Visitor Team", "VisitorPts", "Home Team", "HomePts", "Score Type","OT","OT?", "Notes"]
#print(results.loc[:10])



#提取新特征
results["HomeWin"] = results["VisitorPts"] < results["HomePts"]

#print('----------')
#print(results.loc[:10])

# Our "class values"
y_true = results["HomeWin"].values



results["HomeLastWin"] = False
results["VisitorLastWin"] = False
#print(results.loc[:25])


# Now compute the actual values for these
# Did the home and visitor teams win their last game?
from collections import defaultdict
won_last = defaultdict(int)

for index, row in results.iterrows():
    home_team = row["Home Team"]
    visitor_team = row["Visitor Team"]
	
    row["HomeLastWin"] = won_last[home_team]
    row["VisitorLastWin"] = won_last[visitor_team]
    results.iloc[index] = row    
    # Set current win
    won_last[home_team] = row["HomeWin"]
    won_last[visitor_team] = not row["HomeWin"]

#print('-----------------')
#print(results.loc[:25])


#decision tree

from sklearn.tree import DecisionTreeClassifier
clf = DecisionTreeClassifier(random_state=14)

from sklearn.model_selection import cross_val_score

# Create a dataset with just the neccessary information
X_previouswins = results[["HomeLastWin", "VisitorLastWin"]].values

#print(X_previouswins[:25])

clf = DecisionTreeClassifier(random_state=14)
scores = cross_val_score(clf, X_previouswins, y_true, scoring='accuracy')
print("-------------------")
print("Accuracy: {0:.1f}%".format(np.mean(scores) * 100))


'''
# What about win streaks?
results["HomeWinStreak"] = 0
results["VisitorWinStreak"] = 0
# Did the home and visitor teams win their last game?
from collections import defaultdict
win_streak = defaultdict(int)

for index, row in results.iterrows():  # Note that this is not efficient
    home_team = row["Home Team"]
    visitor_team = row["Visitor Team"]
    row["HomeWinStreak"] = win_streak[home_team]
    row["VisitorWinStreak"] = win_streak[visitor_team]
    results.iloc[index] = row    
    # Set current win
    if row["HomeWin"]:
        win_streak[home_team] += 1
        win_streak[visitor_team] = 0
    else:
        win_streak[home_team] = 0
        win_streak[visitor_team] += 1

print(results.iloc[:25])
'''


# Let's try see which team is better on the ladder. Using the previous year's ladder
ladder_filename = os.path.join("C:\python\data", "standings.csv")
ladder = pd.read_csv(ladder_filename)  #don't skip row 0 and 1


# We can create a new feature -- HomeTeamRanksHigher\
results["HomeTeamRanksHigher"] = 0
for index, row in results.iterrows():
    home_team = row["Home Team"]
    visitor_team = row["Visitor Team"]

    if home_team == "New Orleans Pelicans":
        home_team = "New Orleans Hornets"
    elif visitor_team == "New Orleans Pelicans":
        visitor_team = "New Orleans Hornets"

    home_rank = ladder[ladder["Team"] == home_team]["Rk"].values[0]
    visitor_rank = ladder[ladder["Team"] == visitor_team]["Rk"].values[0]

    row["HomeTeamRanksHigher"] = int(home_rank > visitor_rank)
    results.iloc[index] = row
#results[:5]
#print(results.iloc[:25])



X_homehigher =  results[["HomeLastWin", "VisitorLastWin", "HomeTeamRanksHigher"]].values
clf = DecisionTreeClassifier(random_state=14)
scores = cross_val_score(clf, X_homehigher, y_true, scoring='accuracy')
print("-------------------")
print("Accuracy: {0:.1f}%".format(np.mean(scores) * 100))

#print(results.iloc[:25])
# Who won the last match? We ignore home/visitor for this bit
last_match_winner = defaultdict(int)
results["HomeTeamWonLast"] = 0

for index, row in results.iterrows():
    home_team = row["Home Team"]
    visitor_team = row["Visitor Team"]
    teams = tuple(sorted([home_team, visitor_team]))  # Sort for a consistent ordering
    #不考虑主场/客场，但记录该特征时，只保存主场赢的情况
    row["HomeTeamWonLast"] = 1 if last_match_winner[teams] == row["Home Team"] else 0
    #更新原数据
    results.iloc[index] = row

    #字典中保存赢家。比如{(A,B):A,(C,B):B,....}
    winner = row["Home Team"] if row["HomeWin"] else row["Visitor Team"]
    last_match_winner[teams] = winner
print(results.iloc[:100])

X_home_higher =  results[["HomeLastWin", "VisitorLastWin","HomeTeamRanksHigher", "HomeTeamWonLast"]].values
clf = DecisionTreeClassifier(random_state=14)
scores = cross_val_score(clf, X_home_higher, y_true, scoring='accuracy')
print("-------------------")
print("Accuracy: {0:.1f}%".format(np.mean(scores) * 100))

'''
from sklearn.grid_search import GridSearchCV

parameter_space = {
                   "max_depth": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20],
                   }
clf = DecisionTreeClassifier(random_state=14)
grid = GridSearchCV(clf, parameter_space)
grid.fit(X_homehigher, y_true)
print("Accuracy: {0:.1f}%".format(grid.best_score_ * 100))

'''




'''
from sklearn.preprocessing import LabelEncoder, OneHotEncoder

#建立转换器对象
encoding = LabelEncoder()
#球队名转换为整数
encoding.fit(results["Home Team"].values)
#对主场/客场球队进行标准化（其实，根据fit，转换整数）
home_teams = encoding.transform(results["Home Team"].values)
visitor_teams = encoding.transform(results["Visitor Team"].values)

X_teams = np.vstack([home_teams, visitor_teams]).T
print(X_teams)
'''
'''
onehot = OneHotEncoder()
X_teams = onehot.fit_transform(X_teams).todense()

clf = DecisionTreeClassifier(random_state=14)
scores = cross_val_score(clf, X_teams, y_true, scoring='accuracy')
print("-------------------")
print("Accuracy: {0:.1f}%".format(np.mean(scores) * 100))
'''

'''
from sklearn.ensemble import RandomForestClassifier
clf = RandomForestClassifier(random_state=14)
scores = cross_val_score(clf, X_teams, y_true, scoring='accuracy')
print("-------------------")
print("Accuracy: {0:.1f}%".format(np.mean(scores) * 100))
'''

'''
X_all = np.hstack([X_home_higher, X_teams])
print(X_all.shape)


clf = RandomForestClassifier(random_state=14)
scores = cross_val_score(clf, X_all, y_true, scoring='accuracy')
print("Using whether the home team is ranked higher")
print("Accuracy: {0:.1f}%".format(np.mean(scores) * 100))


#n_estimators=10, criterion='gini', max_depth=None, 
#min_samples_split=2, min_samples_leaf=1,
#max_features='auto',
#max_leaf_nodes=None, bootstrap=True,
#oob_score=False, n_jobs=1,
#random_state=None, verbose=0, min_density=None, compute_importances=None
parameter_space = {
                   "max_features": [2, 10, 'auto'],
                   "n_estimators": [100,],
                   "criterion": ["gini", "entropy"],
                   "min_samples_leaf": [2, 4, 6],
                   }
clf = RandomForestClassifier(random_state=14)
grid = GridSearchCV(clf, parameter_space)
grid.fit(X_all, y_true)
print("Accuracy: {0:.1f}%".format(grid.best_score_ * 100))
print(grid.best_estimator_)
'''
